import api from "src/config/axiosConfigs";
import { IAppPagination, ISortResult } from "src/context/app-context";
import { PropertyFilter } from "src/models/PropertyFilter";

export default class PropertyService {
  async getPropertyList() {
    return await api.get(`properties/`);
  }

  async getPropertyByDetail(propertyId: number) {
    return await api.get(`properties/${propertyId}`);
  }

  async getFeatureProperties() {
    return await api.get(`properties?_limit=3`);
  }

  async getPropertiesByFilter(
    filter: PropertyFilter,
    appPagination: IAppPagination,
    sortResult: ISortResult
  ) {
    const end = appPagination.activePage * appPagination.itemsPerPage;
    const start = end - appPagination.itemsPerPage;

    let url: string = `properties?title_like=${filter.title}&price_gte=${filter.minPrice}&price_lte=${filter.maxPrice}&_start=${start}&_end=${end}`;
    if (sortResult.price) {
      url = url + `&_sort=price&_order=${sortResult.price}`;
    }

    return await api.get(url);
  }
}
