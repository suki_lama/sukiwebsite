import api from "src/config/axiosConfigs";

export default class NewsService{
      async getNewsList() {
        return await api.get(`news?_limit=4`);
      }
      async getNewsByDetail(newsId: number) {
        return await api.get(`news/${newsId}`);
      }
      
}