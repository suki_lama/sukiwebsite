import api from "src/config/axiosConfigs";

export default class ProductService {
  async getProductList() {
    return await api.get(`products?_limit=4`);
  }
  async getProductByDetail(productId: number) {
    return await api.get(`products/${productId}`);
  }
 
}
