export interface Property {
  id: number;
  title: string;
  category_id: number;
  status: string;
  price: number;
  bedroom_count: number;
  bathroom_count: number;
  thumbnail: string;
  kitchen_count: number;
  parking_space_count: number;
  location: Location;
  description: string;
  slug: string;
  gpslat: string;
  gpslng: string;
}

export const initialPropertyState: Property = {
  id: 0,
  title: "",
  category_id: 0,
  status: "",
  price: 0,
  bedroom_count: 0,
  bathroom_count: 0,
  thumbnail: "",
  kitchen_count: 0,
  parking_space_count: 0,
  description: "",
  slug: "",
  gpslat: "",
  gpslng: "",
  location: {
    area: "",
    city: "",
    city_id: 0
  }
};

export interface Location {
  city: string;
  area: string;
  city_id: number
}
