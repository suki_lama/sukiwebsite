import React from "react";
import Pagination from "react-bootstrap/Pagination";
import { AppContext, AppContextType } from "src/context/app-context";

export interface Props {
  totalSearchResult: any;
  handleSetActivePage: any;
}

export const AppPagination = ({
  totalSearchResult,
  handleSetActivePage,
}: Props) => {
  const { appPagination, updateAppPagination } = React.useContext(
    AppContext
  ) as AppContextType;

  const totalPage = Math.ceil(totalSearchResult / appPagination.itemsPerPage);
  let items = [];
  for (let number = 1; number <= totalPage; number++) {
    items.push(
      <Pagination.Item
        key={number}
        active={number === appPagination.activePage}
        onClick={(e) =>
          updateAppPagination({ ...appPagination, activePage: number })
        }
      >
        {number}
      </Pagination.Item>
    );
  }
  return (
    <div>
      <Pagination size="lg">{items}</Pagination>
    </div>
  );
};
