import React from "react";
import { Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { Property } from "src/models/Property";

export const PropertyItem = ({ ...property }: Property) => {
  let navigate = useNavigate();
  const handleShowPropertyDetail = () => {
    navigate({
      pathname: `/property-detail/${property.slug}`,
    });
  };
  return (
    <Card style={{ width: "18rem", marginBottom: "4rem" }}>
      <Card.Img variant="top" src={property.thumbnail} height="200" />

      <Card.Body>
        <Card.Title
          onClick={handleShowPropertyDetail}
          style={{ cursor: "pointer" }}
        >
          {property.title.substring(0, 50)}
        </Card.Title>
        <Card.Text>{property.description.substring(0, 50)}</Card.Text>
        <h4> NPR. {property.price.toLocaleString("en-IN")}</h4>
      </Card.Body>
    </Card>
  );
};
