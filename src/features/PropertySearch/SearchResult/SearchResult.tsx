import React, { useEffect, useState } from "react";
import { Col, Row, Dropdown, Form } from "react-bootstrap";
import { SearchBar } from "../SearchBar/SearchBar";
import { Property } from "../../../models/Property";
import {
  AppContext,
  AppContextType,
  SortType,
} from "../../../context/app-context";
import { AppPagination } from "src/features/PropertySearch";
import PropertyService from "src/services/PropertyService";
import { PropertyItem } from "../PropertyItem/PropertyItem";
import { PropertyFilter } from "src/models/PropertyFilter";

export const SearchResult = () => {
  const {
    searchPropertyFilter,
    appPagination,
    sortResult,
    updateAppPagination,
    updateSortResult,
  } = React.useContext(AppContext) as AppContextType;

  const _propertyService = new PropertyService();

  const [searchProperties, setSearchProperties] = useState<Property[]>([]);
  const [totalSearchResult, setTotalSearchResult] = useState<number>();
  const [handleSetActivePage, setHandleSetActivePage] =
    useState<boolean>(false);

  useEffect(() => {
    fetchProperties();
  }, [searchPropertyFilter]);

  useEffect(() => {
    fetchProperties();
  }, [appPagination]);

  useEffect(() => {
    fetchProperties();
  }, [sortResult]);

  const mapPropertyFilterRequest = () => {
    const propertyFilterRequest: PropertyFilter = {
      title: searchPropertyFilter.title,
      minPrice: searchPropertyFilter.minPrice
        ? searchPropertyFilter.minPrice
        : 0,
      maxPrice: searchPropertyFilter.maxPrice
        ? searchPropertyFilter.maxPrice
        : 999999999,
    };

    return propertyFilterRequest;
  };

  const fetchProperties = async () => {
    const response = await _propertyService.getPropertiesByFilter(
      mapPropertyFilterRequest(),
      appPagination,
      sortResult
    );
    setTotalSearchResult(parseInt(response.headers["x-total-count"]));
    setSearchProperties(response.data);
  };

  const handleSorting = (e: any, sortOrder: any) => {
    e.preventDefault();
    switch (sortOrder) {
      case "PRICEDESC":
        updateSortResult({ ...sortResult, price: SortType.DESC });
        break;
      case "PRICEASC":
        updateSortResult({ ...sortResult, price: SortType.ASC });
        break;
      default:
        break;
    }
  };
  const handleSetItemsPerPage = (e: any) => {
    e.preventDefault();
    if (e.target.value > 0) {
      updateAppPagination({ ...appPagination, itemsPerPage: e.target.value });
    }
  };
  return (
    <div>
      <SearchBar />
      <div className="search-container">
        <div className="search-result">
          <h3 style={{ marginTop: "24px", marginBottom: "24px" }}>
            {totalSearchResult} Search Results
          </h3>
        </div>
        <div className="search-sorting">
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              SORT BY
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item onClick={(e) => handleSorting(e, "PRICEDESC")}>
                Highest Price
              </Dropdown.Item>
              <Dropdown.Item onClick={(e) => handleSorting(e, "PRICEASC")}>
                Lowest Price
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>

      <Row>
        <Col sm="9">
          <AppPagination
            totalSearchResult={totalSearchResult}
            handleSetActivePage={handleSetActivePage}
          />
        </Col>
        <Col>
          <Form.Select
            aria-label="Default select example"
            onClick={(e) => handleSetItemsPerPage(e)}
          >
            <option value="0">Items per page</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="40">40</option>
          </Form.Select>
        </Col>
      </Row>
      <Row>
        {searchProperties.map((property) => {
          return (
            <Col key={property.id} lg="4" sm="12">
              <PropertyItem key={property.id} {...property} />
            </Col>
          );
        })}
      </Row>
    </div>
  );
};
