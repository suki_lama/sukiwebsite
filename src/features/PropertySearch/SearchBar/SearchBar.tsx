import React, { useEffect, useState } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { AppContext, AppContextType } from "../../../context/app-context";

export const SearchBar = () => {
  const { propertyFilter, updatePropertyFilter, updateSearchPropertyFilter } =
    React.useContext(AppContext) as AppContextType;

  let navigate = useNavigate();

  const search = useLocation().search;

  const setFilter = (type: string, value: any) => {
    switch (type) {
      case "title":
        updatePropertyFilter({ ...propertyFilter, title: value });
        break;
      case "minPrice":
        updatePropertyFilter({ ...propertyFilter, minPrice: value });
        break;
      case "maxPrice":
        updatePropertyFilter({ ...propertyFilter, maxPrice: value });
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    updatePropertyFilter({
      title: new URLSearchParams(search).get("title"),
      minPrice: new URLSearchParams(search).get("minPrice"),
      maxPrice: new URLSearchParams(search).get("maxPrice"),
    });
  }, []);

  const handleSubmit = (e: any) => {
    e.preventDefault();

    if (!propertyFilter.title) {
      propertyFilter.title = "";
    }

    if (!propertyFilter.minPrice) {
      propertyFilter.minPrice = "";
    }

    if (!propertyFilter.maxPrice) {
      propertyFilter.maxPrice = "";
    }

    if (propertyFilter.maxPrice > 9999999999 && propertyFilter.minPrice < 0) {
      alert(
        "Please enter Maximum amount upto 10 digit & Minimum value should not be in minus"
      );
      return;
    }

    updateSearchPropertyFilter();

    navigate({
      pathname: "/property-search",
      search: `?title=${propertyFilter.title}&minPrice=${propertyFilter.minPrice}&maxPrice=${propertyFilter.maxPrice}`,
    });
  };

  return (
    <div>
      <div className="home-search">
        <form onSubmit={(e) => handleSubmit(e)}>
          <Row>
            <Col>
              <Form.Control
                placeholder="Search Here"
                value={propertyFilter.title}
                onChange={(e) => {
                  setFilter("title", e.target.value);
                }}
              />
            </Col>
            <Col>
              <Form.Control
                type="number"
                min="0"
                placeholder="Minimum Price"
                value={propertyFilter.minPrice}
                onChange={(e) => {
                  setFilter("minPrice", parseInt(e.target.value));
                }}
              />
            </Col>
            <Col>
              <Form.Control
                type="number"
                min="0"
                placeholder="Maximum Price"
                value={propertyFilter.maxPrice}
                onChange={(e) => {
                  setFilter("maxPrice", parseInt(e.target.value));
                }}
              />
            </Col>
            <Col>
              <Button variant="primary" type="submit">
                Search
              </Button>
            </Col>
          </Row>
        </form>
      </div>
    </div>
  );
};
