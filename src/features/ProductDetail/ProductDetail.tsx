import React from "react";
import { useLocation, useParams } from "react-router-dom";
import { Col, Row, Image } from "react-bootstrap";
import { MdOutlineBedroomChild } from "react-icons/md";
import { GiBathtub } from "react-icons/gi";
import { TbToolsKitchen2 } from "react-icons/tb";
import { AiFillCar } from "react-icons/ai";
import { GrLocation } from "react-icons/gr";
import { useGetProductDetailByIdQuery } from "./ProductDetailApi";

export const ProductDetail = () => {
  let params = useParams();

  const getProductId = () => {
    let pid: any = params.id;
    return pid;
  };
  const { data, error, isLoading } = useGetProductDetailByIdQuery(
    getProductId()
  );

  return (
    <>
      {error ? (
        <>Oh no, there was an error</>
      ) : isLoading ? (
        <>Loading...</>
      ) : data ? (
        <>
          <div>
            <Row>
              <Col sm="6">
                <h4>{data.name}</h4>
                <Row>
                  <Col>
                    <MdOutlineBedroomChild size="35" />
                    <h5>Bedroom </h5>
                  </Col>

                  <Col>
                    <GiBathtub size="35" />
                    <h5>Bathroom </h5>
                  </Col>

                  <Col>
                    <TbToolsKitchen2 size="35" />
                    <h5>Kitchen </h5>
                  </Col>
                  <Col>
                    <AiFillCar size="35" />
                    <h5>Parking</h5>
                  </Col>
                  <Col>
                    <GrLocation size="35" />
                    <h5>Location</h5>
                  </Col>
                </Row>
              </Col>
              <Col sm="6">
                <Image fluid={true} src={data.image} />
              </Col>
            </Row>
          </div>
        </>
      ) : null}
    </>
  );
};
