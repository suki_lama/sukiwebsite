import { Product } from "src/models/Product";
import { emptySplitApi } from "src/redux-utils/emptySplitApi";

export const productDetailApi = emptySplitApi.injectEndpoints({
  endpoints: (builder) => ({
    getProductDetailById: builder.query<Product, number>({
      query: (productId) => `products/${productId}`,
    }),
  }),
});

export const { useGetProductDetailByIdQuery } = productDetailApi;
