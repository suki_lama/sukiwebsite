import React from "react";
import { GoogleMap, LoadScript } from "@react-google-maps/api";
import { Property } from "src/models/Property";

const containerStyle = {
  width: "100%",
  height: "30rem",
};

export const GoogleMapCustom = (property: Property) => {
  const center = {
    lat: parseFloat(property.gpslat),
    lng: parseFloat(property.gpslng),
  };
  return (
    <>
      {/* <LoadScript googleMapsApiKey="AIzaSyC3TdMZ6qRlsGqYS1-lK0N_s5AR6Nh6Ml0"> */}
      <LoadScript googleMapsApiKey="AIzaSyC3TdMZ6qRlsGqYS1-lK0N_s5AR6Nh6Ml0">
        <GoogleMap mapContainerStyle={containerStyle} center={center} zoom={14}>
          {/* Child components, such as markers, info windows, etc. */}
          <></>
        </GoogleMap>
      </LoadScript>
    </>
  );
};
