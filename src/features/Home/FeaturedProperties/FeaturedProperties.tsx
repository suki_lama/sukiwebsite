import React, { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
// import { PropertyItem } from "../../PropertySearch/PropertyItem/PropertyItem";

import { PropertyItem } from "src/features/PropertySearch";
import { Property } from "src/models/Property";
import PropertyService from "src/services/PropertyService";

export const FeaturedProperties = () => {
  const _propertyService = new PropertyService();
  const response = _propertyService.getFeatureProperties();
  const [featuredProperties, setFeaturedProperties] = useState([]);

  const fetchFeaturedProperties = async () => {
    setFeaturedProperties((await response).data);
  };

  useEffect(() => {
    fetchFeaturedProperties();
  }, []);

  return (
    <Row>
      {featuredProperties &&
        featuredProperties.map((property: Property) => {
          return (
            <Col key={property.id} lg="4" sm="12">
              <PropertyItem key={property.id} {...property} />
            </Col>
          );
        })}
    </Row>
  );
};
