import React, { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import NewsService from "src/services/NewsService";
import { LatestNews } from "../LatestNews/LatestNews";

export const NewsList = () => {

  const [currentNews, setNews] = useState([]);

  const fetchNewsList = async () => {
    const newsService = new NewsService();
    const response = await newsService.getNewsList();
    setNews(response.data);
  };

  useEffect(() => {
    fetchNewsList();
  }, []);

  return (
    <Row>
      {currentNews && currentNews.map((news: any) => {
        return (
          <Col key={news.id} lg="3" sm="12">
            <LatestNews key={news.id} news={news} />
          </Col>
        );
      })}
    </Row>
  );
};
