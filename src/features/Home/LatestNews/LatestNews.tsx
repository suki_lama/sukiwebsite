import React from "react";
import { useNavigate } from "react-router-dom";
import { Image } from "react-bootstrap"

export const LatestNews = ({ news }: any) => {
  let navigate = useNavigate();
  const handleShowNewsDetail = () => {
    navigate({
      // pathname: "/news-detail",
      // search: `${news.id}`,
      pathname: `/news-detail/${news.id}`,
    });
  };
  return (
    <div>
      <div className="service-div">
        <Image fluid={true} src={news.image} alt="images" width="304px" height="200px" />
        <br />
        <p onClick={handleShowNewsDetail}
          style={{ cursor: "pointer" }}>{news.details}</p>
      </div>
    </div>
  );
};
