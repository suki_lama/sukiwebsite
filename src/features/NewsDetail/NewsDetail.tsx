import React, { useEffect, useState } from 'react'
import axios from "axios";
import { useLocation, useParams } from 'react-router-dom';
import NewsService from 'src/services/NewsService';
import { News } from "src/models/News";
import { Col, Row, Image } from 'react-bootstrap';
import { MdOutlineBedroomChild } from 'react-icons/md'
import { GiBathtub } from 'react-icons/gi'
import { TbToolsKitchen2 } from 'react-icons/tb'
import { AiFillCar } from 'react-icons/ai'
import { GrLocation } from 'react-icons/gr'


export const NewsDetail = () => {
    const initialNewsState: News = {
        id: 0,
        details: "",
        image: ""
    }
    const [newsId, setNewsId] = useState<number>();
    const [news, setNews] = useState<News>(initialNewsState);

    const _newsService = new NewsService();
    let params = useParams();


    useEffect(() => {
        let nid: any = params.id;

        setNewsId(parseInt(nid));

    }, []);

    useEffect(() => {
        fetchNewsDetail();
    }, [newsId])

    const search = useLocation().search;

    const fetchNewsDetail = async () => {
        if (newsId && newsId > 0) {
            try {
                const response = await _newsService.getNewsByDetail(newsId)
                setNews(response.data);
            } catch (e) {
                alert(e);
            }
        }
    }
    return (
        <div>
            <Row>
                <Col sm="6">
                    <h4>{news.details}</h4>
                    <Row>
                        <Col>
                            <MdOutlineBedroomChild size="35" />
                            <h5>Bedroom </h5>
                        </Col>

                        <Col>
                            <GiBathtub size="35" />
                            <h5>Bathroom </h5>
                        </Col>

                        <Col>
                            <TbToolsKitchen2 size="35" />
                            <h5>Kitchen </h5>
                        </Col>
                        <Col>
                            <AiFillCar size="35" />
                            <h5>Parking</h5>
                        </Col>
                        <Col>
                            <GrLocation size="35" />
                            <h5>Location</h5>
                        </Col>
                    </Row>
                </Col>
                <Col sm="6"><Image fluid={true} src={news.image} /></Col>
            </Row>
        </div>
    )
}
