import React from "react";
import { useNavigate } from "react-router-dom";
import { Product } from "src/models/Product";
import { Image } from 'react-bootstrap';

export const ProductItem = (product: Product) => {
  let navigate = useNavigate();
  const handleShowProductDetail = () => {
    navigate({
      pathname: `/product-detail/${product.id}`,
    });
  };
  return (
    <div className="service-div">
      <Image fluid={true} src={product.image} alt="images" width="305px" height="200px" />
      <br />
      <p onClick={handleShowProductDetail} style={{ cursor: "pointer" }}>
        {product.name}
      </p>
    </div>
  );
};
