import React, { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import ProductService from "src/services/ProductService";
import { Product } from "src/models/Product";
import { ProductItem } from "../ProductItem/ProductItem";

export const ProductList = () => {

  const [products, setProducts] = useState<Product[]>([]);


  useEffect(() => {
    fetchProducts();
  }, [])

  const fetchProducts = async () => {
    const _productService = new ProductService();
    const response = await _productService.getProductList();
    setProducts(response.data);
  }

  return (
    <Row>
      {products.map((service: Product) => {
        return (
          <Col key={service.id} lg="3" sm="2">
            <ProductItem key={service.id} {...service} />
          </Col>
        );
      })}
    </Row>
  );
};
