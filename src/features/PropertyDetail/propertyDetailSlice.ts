import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { initialPropertyState } from "src/models/Property";
import { RootState } from "src/redux-utils/store";
import PropertyService from "src/services/PropertyService";

const _propertyService = new PropertyService();

const initialState = {
  property: initialPropertyState,
  loading: true,
};

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched. Thunks are
// typically used to make async requests.
export const fetchPropertyDetailAsync = createAsyncThunk(
  "propertyDetail/fetch/",
  async (propertyId: number) => {
    const response = await _propertyService.getPropertyByDetail(propertyId);
    // The value we return becomes the `fulfilled` action payload
    return response.data;
  }
);

export const propertyDetailSlice = createSlice({
  name: "propertyDetail",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    updatePrice: (state) => {
      state.property.price = 60000;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchPropertyDetailAsync.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchPropertyDetailAsync.fulfilled, (state, action) => {
        state.property = action.payload;
        state.loading = false;
      })
      .addCase(fetchPropertyDetailAsync.rejected, (state) => {
        state.loading = false;
      });
  },
});

export const { updatePrice } = propertyDetailSlice.actions;

export const selectPrice = (state: RootState) =>
  state.propertyDetail.property.price;
export const selectProperty = (state: RootState) =>
  state.propertyDetail.property;
export const selectLoading = (state: RootState) => state.propertyDetail.loading;

export default propertyDetailSlice.reducer;
