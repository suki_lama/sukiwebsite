import { emptySplitApi } from "src/redux-utils/emptySplitApi";
import { Property } from "src/models/Property";

export const propertyDetailApi = emptySplitApi.injectEndpoints({
  endpoints: (builder) => ({
    getPropertyDetailById: builder.query<Property, number>({
      query: (propertyId) => `properties/${propertyId}`,
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetPropertyDetailByIdQuery } = propertyDetailApi;
