import axios from "axios";
import React, { useEffect, useState } from "react";
import "./styles.scss";
import { useLocation, useParams } from "react-router-dom";
import { MdOutlineBedroomChild } from "react-icons/md";
import { GiBathtub } from "react-icons/gi";
import { Col, Collapse, Row, Image } from "react-bootstrap";
import { TbToolsKitchen2 } from "react-icons/tb";
import { AiFillCar } from "react-icons/ai";
import { GrLocation } from "react-icons/gr";
import parse from "html-react-parser";
import { GoogleMapCustom } from "../GoogleMapCustom";
import { useAppDispatch, useAppSelector } from "src/redux-utils/hooks";
import {
  updatePrice,
  selectPrice,
  fetchPropertyDetailAsync,
  selectProperty,
  selectLoading,
} from "./propertyDetailSlice";
import { useGetPropertyDetailByIdQuery } from "src/features/PropertyDetail/PropertyDetailApi";

export const PropertyDetail = () => {
  const [propertyId, setPropertyId] = useState<number>();
  const dispatch = useAppDispatch();

  let params = useParams();

  const price = useAppSelector(selectPrice);
  const loading = useAppSelector(selectLoading);
  
  const getPropertyIdFromUrl = () => {
    let slug: any = params.slug;
    let slugSplit = slug.split("-");
    let pid = slugSplit[slugSplit.length - 1];
    return pid;
  };

  const { data, error, isLoading } = useGetPropertyDetailByIdQuery(
    getPropertyIdFromUrl()
  );

  useEffect(() => {
    if (propertyId) {
      dispatch(fetchPropertyDetailAsync(propertyId));
    }
  }, [propertyId]);

  const search = useLocation().search;

  return (
    <>
      {error ? (
        <>Oh no, there was an error</>
      ) : isLoading ? (
        <>Loading...</>
      ) : data ? (
        <>
          <div className="property-detail__section">
            <div className="property-detail__container">
              <div className="property-detail__title">
                <Row>
                  <h1>{data.title}</h1>
                  <button onClick={() => dispatch(updatePrice())}>
                    Update price via redux {price}
                  </button>
                </Row>
              </div>
              <Row>
                <h4>Price: NPR.{data.price.toLocaleString("en-IN")}</h4>
              </Row>
              <Row>
                <Col sm="6">
                  <Row>
                    <Col>
                      <MdOutlineBedroomChild size="35" />
                      <h5>Bedroom {data.bedroom_count}</h5>
                    </Col>

                    <Col>
                      <GiBathtub size="35" />
                      <h5>Bathroom {data.bathroom_count}</h5>
                    </Col>

                    <Col>
                      <TbToolsKitchen2 size="35" />
                      <h5>Kitchen {data.kitchen_count}</h5>
                    </Col>
                    <Col>
                      <AiFillCar size="35" />
                      <h5>Parking{data.parking_space_count}</h5>
                    </Col>
                    <Col>
                      <GrLocation size="35" />
                      <h5>Location{data.location?.city}</h5>
                    </Col>
                  </Row>

                  <Row>
                    <h4>Description</h4>
                    <p>{parse(data.description)}</p>
                  </Row>
                </Col>
                <Col sm="6">
                  <div className="property-detail__image">
                    <Image fluid={true} src={data.thumbnail} alt="image" />
                  </div>
                </Col>
              </Row>

              <Row>{data.id > 0 && <GoogleMapCustom {...data} />}</Row>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
};
