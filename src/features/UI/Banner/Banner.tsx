import React from "react";
import { SearchBar } from "../../PropertySearch/SearchBar/SearchBar";

export const Banner = () => {
  return (
    <div className="home-heading">
      <h1>No. 1 Property Market Place in Nepal</h1>
      <SearchBar />
    </div>
  );
};
