import React from "react";
import { Button, Container, Navbar, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export const Navigation: React.FC = () => {
  let navigate = useNavigate();

  const goToHome = (e: any) => {
    e.preventDefault();
    navigate({ pathname: "/" });
  };

  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand onClick={goToHome}>Suki RealEstate</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            Signed in as: <a href="#login">Satish</a>
            <Button>Change User</Button>
          </Navbar.Text>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};
