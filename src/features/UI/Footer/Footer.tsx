import React from "react";
import { GrInstagram } from "react-icons/gr";
import { FaFacebook } from "react-icons/fa";
import { SiTwitter } from "react-icons/si";
import { SiLinkedin } from "react-icons/si";
import { SiGmail } from "react-icons/si";

export const Footer = () => {
  return (
    <div className="footer-container">
      <div className="footer-details">
        <span>
          <GrInstagram size="25px" />
        </span>
        <span>
          <FaFacebook size="25px" />
        </span>
        <span>
          <SiTwitter size="25px" />
        </span>
        <span>
          <SiLinkedin size="25px" />
        </span>
        <span>
          <SiGmail size="25px" />
        </span>
        <br />
        <p style={{ color: "green" }}>sukilama222@gmail.com</p>
        <p>
          This site is protected by reCAPTCHA Enterprise and the Google Privacy
          Policy and Terms of Service apply.
        </p>
      </div>
    </div>
  );
};
