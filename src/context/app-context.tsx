import React, { useState } from "react";
import { PropertyFilter } from "../models/PropertyFilter";

export interface IUser {
  firstName: string;
  lastName: string;
}

export interface IAppPagination {
  activePage: number;
  itemsPerPage: number;
}

export interface ISortResult {
  price: string;
}

export enum SortType {
  ASC = "asc",
  DESC = "desc",
  DEFAULT = "",
}

export type AppContextType = {
  user: IUser;
  propertyFilter: PropertyFilter;
  searchPropertyFilter: PropertyFilter;
  appPagination: IAppPagination;
  sortResult: ISortResult;
  updatePropertyFilter: (propertyFilter: PropertyFilter) => void;
  updateUser: (user: IUser) => void;
  updateSearchPropertyFilter: () => void;
  updateAppPagination: (appPagination: IAppPagination) => void;
  updateSortResult: (sortResult: ISortResult) => void;
};

export const AppContext = React.createContext<AppContextType | null>(null);

const AppContextProvider = ({ children }: any) => {
  const [user, setUser] = useState<IUser>({ firstName: "", lastName: "" });

  const [propertyFilter, setPropertyFilter] = useState<PropertyFilter>({
    maxPrice: "",
    minPrice: "",
    title: "",
  });

  const [searchPropertyFilter, setSearchPropertyFilter] =
    useState<PropertyFilter>({
      maxPrice: "",
      minPrice: "",
      title: "",
    });

  const [appPagination, setAppPagination] = useState<IAppPagination>({
    activePage: 1,
    itemsPerPage: 10,
  });

  const [sortResult, setSortResult] = useState<ISortResult>({
    price: SortType.DEFAULT,
  });

  const updateUser = (userParam: IUser) => {
    setUser(userParam);
  };

  const updatePropertyFilter = (propertyFilter: PropertyFilter) => {
    if (isNaN(propertyFilter.minPrice)) {
      propertyFilter.minPrice = 0;
    }

    if (isNaN(propertyFilter.maxPrice)) {
      propertyFilter.maxPrice = 9999999999;
    }

    setPropertyFilter(propertyFilter);
  };

  const updateSearchPropertyFilter = () => {
    setSearchPropertyFilter(propertyFilter);
  };

  const updateAppPagination = (appPagination: IAppPagination) => {
    setAppPagination(appPagination);
  };

  const updateSortResult = (sortResult: ISortResult) => {
    setSortResult(sortResult);
  };

  return (
    <AppContext.Provider
      value={{
        user,
        propertyFilter,
        searchPropertyFilter,
        appPagination,
        sortResult,
        updatePropertyFilter,
        updateUser,
        updateSearchPropertyFilter,
        updateAppPagination,
        updateSortResult,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export default AppContextProvider;
