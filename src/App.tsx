import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import "./styles.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import Router from "./router/Router";
import AppContextProvider from "./context/app-context";

export default function App() {
  return (
    <Container>
      <AppContextProvider>
        {/* <React.StrictMode> */}
        <Router />
        {/* </React.StrictMode> */}
      </AppContextProvider>
    </Container>
  );
}
