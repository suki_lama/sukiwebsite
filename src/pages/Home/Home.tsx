import React from "react";
import { FeaturedProperties, NewsList } from "src/features/Home";
import { ProductList } from "src/features/Product";
import { Banner } from "src/features/UI";

export const Home = () => {
  return (
    <div>
      <Banner />
      <div>
        <h3> Featured Homes</h3>
        <FeaturedProperties />
        <h3> Product Services</h3>
        <ProductList />
        <h3>Latest News</h3>
        <NewsList />
      </div>
    </div>
  );
};
