import React, { lazy, Suspense } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Footer } from "../features/UI/Footer/Footer";
import { Home } from "../pages/Home/Home";
import { Navigation } from "src/features/UI/Navigation/Navigation";
import { SearchResult } from "src/features/PropertySearch/SearchResult/SearchResult";

const Router = () => {
  const ProductDetailPage = lazy(() =>
    import("src/features/ProductDetail/ProductDetail").then((module) => ({
      default: module.ProductDetail,
    }))
  );

  const PropertyDetailPage = lazy(() =>
    import("src/features/PropertyDetail/PropertyDetail").then((module) => ({
      default: module.PropertyDetail,
    }))
  );

  const NewsDetailPage = lazy(() =>
    import("src/features/NewsDetail/NewsDetail").then((module) => ({
      default: module.NewsDetail,
    }))
  );

  return (
    <BrowserRouter>
      <Navigation />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="property-search" element={<SearchResult />}></Route>
        <Route path="property-detail">
          <Route
            path=":slug"
            element={
              <Suspense>
                <PropertyDetailPage />
              </Suspense>
            }
          />
        </Route>
        <Route path="product-detail">
          <Route
            path=":id"
            element={
              <Suspense>
                <ProductDetailPage />
              </Suspense>
            }
          />
        </Route>
        <Route path="news-detail">
          <Route
            path=":id"
            element={
              <Suspense>
                <NewsDetailPage />
              </Suspense>
            }
          />
        </Route>
      </Routes>
      <Footer />
    </BrowserRouter>
  );
};

export default Router;
